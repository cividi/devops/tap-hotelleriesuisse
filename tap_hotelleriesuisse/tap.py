"""hotelleriesuisse tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th

from tap_hotelleriesuisse.streams import (
    hotelleriesuisseStream,
    HotelsStream
)

STREAM_TYPES = [
    HotelsStream,
]


class Taphotelleriesuisse(Tap):
    """hotelleriesuisse tap class."""
    name = "tap-hotelleriesuisse"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "auth_username",
            th.StringType,
            required=True,
            description="The username to authenticate against the API service"
        ),
        th.Property(
            "auth_password",
            th.StringType,
            required=True,
            secret=True,  # Flag config as protected.
            description="The password to authenticate against the API service"
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync"
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://portal.swisshoteldata.ch",
            description="The url for the API service"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    Taphotelleriesuisse.cli()
