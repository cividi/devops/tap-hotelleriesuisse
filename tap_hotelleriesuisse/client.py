"""Custom client handling, including hotelleriesuisseStream base class."""

import requests
import xmltodict
import datetime
from typing import Optional, Iterable

from singer_sdk.streams import Stream


class hotelleriesuisseStream(Stream):
    """Stream class for hotelleriesuisse streams."""

    @property
    def auth_username(self) -> str:
        return self.config.get("auth_username")

    @property
    def auth_password(self) -> str:
        return self.config.get("auth_password")

    @property
    def start_date(self) -> datetime.datetime:
        return self.config.get("start_date")

    @property
    def url_base(self) -> str:
        return f"{self.config.get('api_url')}/ws/export/exporthoteldata.cfc?wsdl"

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Return a generator of record-type dictionary objects.

        The optional `context` argument is used to identify a specific slice of the
        stream if partitioning is required for the stream. Most implementations do not
        require partitioning and should ignore the `context` argument.
        """

        payload = f"""
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:exp="http://export.ws">
        <soapenv:Header>
            <credential> 
                <username><![CDATA[{self.auth_username}]]></username> 
                <password><![CDATA[{self.auth_password}]]></password>
            </credential>
        </soapenv:Header>
        <soapenv:Body>
            <exp:getHotelData soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                <exp:dRequestDate>{self.start_date}</exp:dRequestDate>
            </exp:getHotelData>
        </soapenv:Body>
        </soapenv:Envelope>"""

        headers = {
            "Content-Type": "text/xml",
            "SOAPAction": '""',
            "Cookie": "CFID=50768746; CFTOKEN=81f8161060793e4b-49129795-DCB6-4E34-F5F6AF06977A1FED",
        }

        data = requests.request(
            "POST", self.url_base, headers=headers, data=payload, timeout=100 * 10 * 60
        )

        try:
            hotelDataDict = xmltodict.parse(data.text)
            for record in hotelDataDict["soapenv:Envelope"]["soapenv:Body"]["ns1:getHotelDataResponse"]["getHotelDataReturn"]["results"]["hotelExport"]:
                yield record
        except:
            raise ConnectionError("Couldn't fetch valid data")
