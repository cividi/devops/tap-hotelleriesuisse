"""Stream type classes for tap-hotelleriesuisse."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_hotelleriesuisse.client import hotelleriesuisseStream

# TODO: consider using JSON schemas instead
# SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")

class HotelsStream(hotelleriesuisseStream):
    """Define custom stream."""

    name = "hotels"
    primary_keys = ["hotelNumber"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("hotelNumber", th.StringType),
        th.Property("name", th.StringType),
        th.Property("additionalname", th.StringType),
        th.Property("language", th.StringType),
        th.Property("banquetParticipantMin", th.IntegerType),
        th.Property("banquetParticipantMax", th.IntegerType),
        th.Property("seminarParticipantMin", th.IntegerType),
        th.Property("seminarParticipantMax", th.IntegerType),
        th.Property("bedCount", th.IntegerType),
        th.Property("roomCount", th.IntegerType),
        th.Property("barrierFreeRoomCount", th.IntegerType),
        th.Property("suiteCounte", th.IntegerType),
        th.Property("doubleRoomCount", th.IntegerType),
        th.Property("singleRoomCount", th.IntegerType),
        th.Property("classificationId", th.IntegerType),
        th.Property("isClassificationSuperior", th.BooleanType),
        th.Property("isClassificationGarni", th.BooleanType),
        th.Property("isClassificationChainhotel", th.BooleanType),
        th.Property("isClassificationServicedApartment", th.BooleanType),
        th.Property("classificationComposedCode", th.StringType),
        th.Property("qualitySealId", th.IntegerType),
        th.Property("tourismRegionId", th.IntegerType),
        th.Property("classificationGastroSuisseId", th.IntegerType),
        th.Property("accessibilitystatus", th.IntegerType),
        th.Property("accessibilityapproveddate", th.DateTimeType),
        th.Property("accessibilitydetaillink", th.StringType),
        th.Property(
            "contactData",
            th.ArrayType(
                th.ObjectType(
                    th.Property("position", th.StringType),
                    th.Property("salutation", th.StringType),
                    th.Property("firstname", th.StringType),
                    th.Property("lastname", th.StringType),
                    th.Property("email", th.StringType),
                    th.Property("mobile", th.StringType),
                    th.Property("telefax", th.StringType),
                    th.Property("language", th.StringType),
                ),
            ),
        ),
        th.Property(
            "address",
            th.ObjectType(
                th.Property("street", th.StringType),
                th.Property("streetAddition", th.StringType),
                th.Property("pobox", th.StringType),
                th.Property("zipcode", th.StringType),
                th.Property("city", th.StringType),
                th.Property("province", th.StringType),
                th.Property("country", th.StringType),
                th.Property("telephone", th.StringType),
                th.Property("telefax", th.StringType),
                th.Property("email", th.StringType),
                th.Property(
                    "website",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("@language", th.StringType),
                            th.Property("#text", th.StringType),
                        )
                    ),
                ),
            ),
        ),
        th.Property(
            "membershipData",
            th.ObjectType(
                th.Property(
                    "membership",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("organisationId", th.IntegerType),
                            th.Property("foreignHotelId", th.StringType),
                            th.Property("status", th.StringType),
                        )
                    ),
                )
            ),
        ),
        th.Property(
            "hotelGroupData",
            th.ObjectType(
                th.Property(
                    "hotelGroupId",
                    th.IntegerType,
                ),
            ),
        ),
        th.Property(
            "orientationPriceData",
            th.ArrayType(
                th.ObjectType(
                    th.Property("orientationPriceId", th.IntegerType),
                    th.Property("value", th.NumberType),
                )
            ),
        ),
        th.Property(
            "buildingData",
            th.ObjectType(
                th.Property(
                    "building",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("buildingId", th.IntegerType),
                            th.Property("value", th.IntegerType),
                        ),
                    ),
                )
            ),
        ),
        th.Property(
            "creditCardData",
            th.ObjectType(
                th.Property(
                    "creditCardId",
                    th.ArrayType(
                        th.IntegerType,
                    ),
                ),
            ),
        ),
        th.Property(
            "infrastructureHotelData",
            th.ObjectType(
                th.Property(
                    "infrastructureHotelId",
                    th.ArrayType(
                        th.IntegerType,
                    ),
                )
            ),
        ),
        th.Property(
            "infrastructureLocationData",
            th.ObjectType(
                th.Property(
                    "infrastructureLocationId",
                    th.ArrayType(
                        th.IntegerType,
                    ),
                )
            ),
        ),
        th.Property(
            "pictureCategoryData",
            th.ObjectType(
                th.Property(
                    "pictureCategory",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("categoryId", th.IntegerType),
                            th.Property("pictureCount", th.IntegerType),
                            th.Property(
                                "name",
                                th.ArrayType(
                                    th.ObjectType(
                                        th.Property("@language", th.StringType),
                                        th.Property("#text", th.StringType),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
        th.Property(
            "pictureData",
            th.ObjectType(
                th.Property(
                    "picture",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("categoryId", th.IntegerType),
                            th.Property("pictureId", th.IntegerType),
                            th.Property("sortNumber", th.IntegerType),
                            th.Property(
                                "title",
                                th.ArrayType(
                                    th.ObjectType(
                                        th.Property("@language", th.StringType),
                                        th.Property("#text", th.StringType),
                                    ),
                                ),
                            ),
                            th.Property(
                                "description",
                                th.ArrayType(
                                    th.ObjectType(
                                        th.Property("@language", th.StringType),
                                        th.Property("#text", th.StringType),
                                    ),
                                ),
                            ),
                            th.Property("urlThumb", th.StringType),
                            th.Property("urlWeb", th.StringType),
                            th.Property("urlMain", th.StringType),
                        ),
                    ),
                ),
            ),
        ),
        th.Property(
            "regionalGroupData",
            th.ObjectType(
                th.Property("regionalGroupId", th.IntegerType),
            ),
        ),
        th.Property(
            "certificateData",
            th.ObjectType(
                th.Property("certificateId", th.IntegerType),
            ),
        ),
        th.Property(
            "accessibilityLabelData",
            th.ObjectType(
                th.Property(
                    "accessibilityCriteriaId",
                    th.ArrayType(
                        th.IntegerType,
                    ),
                )
            ),
        ),
        th.Property(
            "accessibilityCriteriaData",
            th.ObjectType(
                th.Property(
                    "accessibilityCriteriaId",
                    th.ArrayType(
                        th.IntegerType,
                    ),
                )
            ),
        ),
    ).to_dict()
